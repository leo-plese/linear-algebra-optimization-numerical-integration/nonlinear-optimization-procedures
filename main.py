from math import sqrt, sin
from random import uniform

file_for_print = None


class GoalFunction():

    # is_multi_input - False by def; for simplex: True by def (False for iterative funs only)
    def __init__(self, f=None, is_multi_input=False):
        self.f = f
        self.called_times = 0
        self.is_multi_input = is_multi_input

    def evaluate(self, x, count_eval=True):
        if count_eval:
            self.called_times += 1

        if self.is_multi_input:
            return self.f(*x)
        else:
            return self.f(x)


def get_unimodal_interval(F, h, x0):
    m = x = x0
    l = m - h
    r = m + h

    step = 1

    fl = F.evaluate(l)
    fm = F.evaluate(m)
    fr = F.evaluate(r)

    if fm < fr and fm < fl:
        return l, r
    elif fm > fr:
        while fm > fr:
            l = m
            m = r
            fm = fr
            r = x + h * 2 ** step
            step += 1
            fr = F.evaluate(r)
        return l, r
    else:
        while fm > fl:
            r = m
            m = l
            fm = fl
            l = x - h * 2 ** step
            step += 1
            fl = F.evaluate(l)
        return l, r


def golden_ratio_with_interval(F, a, b, e=1e-6):
    k = (sqrt(5) - 1) / 2

    c = b - k * (b - a)
    d = a + k * (b - a)
    fc = F.evaluate(c)
    fd = F.evaluate(d)

    i = 1
    while ((b - a) > e):
        if print_it:
            print()
            print(f"iter {i}:")
            # count_eval=False jer fa i fb se racunaju samo radi prikaza, ali ne za potrebe algoritama
            fa = F.evaluate(a, count_eval=False)
            fb = F.evaluate(b, count_eval=False)
            print(f"a={a}; f(a)={fa}")
            print(f"c={c}; f(c)={fc}")
            print(f"d={d}; f(d)={fd}")
            print(f"b={b}; f(b)={fb}")
        if print_to_file:
            file_for_print.write("\n")
            file_for_print.write(f"iter {i}:\n")
            # count_eval=False jer fa i fb se racunaju samo radi prikaza, ali ne za potrebe algoritama
            fa = F.evaluate(a, count_eval=False)
            fb = F.evaluate(b, count_eval=False)
            file_for_print.write(f"a={a}; f(a)={fa}\n")
            file_for_print.write(f"c={c}; f(c)={fc}\n")
            file_for_print.write(f"d={d}; f(d)={fd}\n")
            file_for_print.write(f"b={b}; f(b)={fb}\n")

        if fc < fd:
            b = d
            d = c
            c = b - k * (b - a)
            fd = fc
            if ((b - a) > e):
                fc = F.evaluate(c)
        else:
            a = c
            c = d
            d = a + k * (b - a)
            fc = fd
            if ((b - a) > e):
                fd = F.evaluate(d)
        i += 1

    return F.called_times, a, b


def golden_ratio_with_point(F, h, x0, e=1e-6):
    a, b = get_unimodal_interval(F, h, x0)
    # print("a, b =",a,b)

    return golden_ratio_with_interval(F, a, b, e)


def golden_ratio(f_goal):
    F = GoalFunction(f_goal)
    with open("zlatni_rez_mode.txt", "r") as zlatni_rez_mode_x0_or_i_file:
        mode = zlatni_rez_mode_x0_or_i_file.readline()
        if mode == "x0":  # mode "x0" - zadana pocetna tocka x0 i korak h
            with open("zlatni_rez_tocka.txt", "r") as zlatni_rez_tocka_file:
                lines = zlatni_rez_tocka_file.readlines()
                lines = [l.split("=") for l in lines]
                e = None
                for l in lines:
                    if l[0].strip() == "e":
                        e = float(l[1].strip())
                    elif l[0].strip() == "h":
                        h = float(l[1].strip())
                    else:  # l[0].strip() == "x0"
                        x0 = float(l[1].strip())
            if e is None:
                F_called_times, a_new, b_new = golden_ratio_with_point(F, h, x0)
            else:
                F_called_times, a_new, b_new = golden_ratio_with_point(F, h, x0, e)


        else:  # mode "i" - zadan interval [a, b]
            with open("zlatni_rez_interval.txt", "r") as zlatni_rez_interval_file:
                lines = zlatni_rez_interval_file.readlines()
                lines = [l.split("=") for l in lines]
                e = None
                for l in lines:
                    if l[0].strip() == "e":
                        e = float(l[1].strip())
                    elif l[0].strip() == "a":
                        a = float(l[1].strip())
                    else:  # l[0].strip() == "b"
                        b = float(l[1].strip())
            if e is None:
                F_called_times, a_new, b_new = golden_ratio_with_interval(F, a, b)
            else:
                F_called_times, a_new, b_new = golden_ratio_with_interval(F, a, b, e)

    x_solution = (a_new + b_new) / 2
    return F_called_times, x_solution


def diff_below_eps(x, xs, e):
    n = len(x)
    for i in range(n):
        if abs(x[i] - xs[i]) > e[i]:
            return False
    return True


def coordinate_search_method(f_goal, h, x0, e=None, is_iterative_f_goal=False):
    e_default = 1e-6

    if e is None:
        e = [e_default for _ in range(len(x0))]

    n = len(x0)

    E = [[1 if i == j else 0 for j in range(n)] for i in range(n)]

    x = x0
    xs = x

    F = GoalFunction()

    counter_print = 0
    counter_print_file = 0
    while True:
        if print_it:
            print(f"------- step {counter_print}:")
            counter_print += 1
        if print_to_file:
            file_for_print.write(f"------- step {counter_print_file}:\n")
            counter_print_file += 1

        xs = x

        for i in range(n):
            e_i = E[i]
            Flambdas = [(lambda param_lambda, ind=j: x[ind] + param_lambda * e_i[ind]) for j in range(n)]

            if not is_iterative_f_goal:
                f_goal_lamm = lambda lamm: f_goal(*tuple([Flambdas[j](lamm) for j in range(n)]))
            else:
                f_goal_lamm = lambda lamm: f_goal([Flambdas[j](lamm) for j in range(n)])

            F.f = f_goal_lamm
            F_called_times, a_lamm, b_lamm = golden_ratio_with_point(F, h[i], 0, e[i])

            a_x = [x[j] + a_lamm * e_i[j] for j in range(n)]
            b_x = [x[j] + b_lamm * e_i[j] for j in range(n)]
            x = [(a_x[j] + b_x[j]) / 2 for j in range(n)]

        if (diff_below_eps(x, xs, e)):
            break

    return F.called_times, (x if len(x) > 1 else x[0])


def coordinate_search(params_file_name, f_goal, is_iterative_f_goal=False):
    with open(params_file_name, "r") as koord_params_file:
        e = None
        lines = koord_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = l[1].strip().split()
                e = [float(el) for el in e]
            elif l[0].strip() == "h":
                h = l[1].strip().split()
                h = [float(el) for el in h]
            else:  # l[0].strip() == "x0"
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]

        if e is None:
            F_called_times, x_solution = coordinate_search_method(f_goal, h, x0,
                                                                  is_iterative_f_goal=is_iterative_f_goal)
        else:
            F_called_times, x_solution = coordinate_search_method(f_goal, h, x0, e=e,
                                                                  is_iterative_f_goal=is_iterative_f_goal)

    return F_called_times, x_solution


def get_simplex_points(x0, h):
    x = [x0]
    n = len(x0)
    for i in range(n):
        x.append([x0[i] + h if i == j else x0[j] for j in range(n)])

    return x


def get_index_h(x_eval):
    ind = 0
    max_val = x_eval[0]
    for i in range(1, len(x_eval)):
        if x_eval[i] > max_val:
            max_val = x_eval[i]
            ind = i
    return ind


def get_index_l(x_eval):
    ind = 0
    min_val = x_eval[0]
    for i in range(1, len(x_eval)):
        if x_eval[i] < min_val:
            min_val = x_eval[i]
            ind = i
    return ind


def get_centroid(x, h_ind):
    n = len(x)
    x_without_h = [x[i] for i in range(n) if i != h_ind]
    n -= 1

    dim = len(x[0])
    xc = [sum([x_without_h[i][d] for i in range(n)]) / n for d in range(dim)]

    return xc[:]


def get_reflection_point(xh, xc, alfa):
    d = len(xc)
    xr = [(1 + alfa) * xc[i] - alfa * xh[i] for i in range(d)]

    return xr[:]


def get_expansion_point(xr, xc, gama):
    d = len(xc)
    xe = [(1 - gama) * xc[i] + gama * xr[i] for i in range(d)]

    return xe[:]


def is_xr_worse_than_all_except_xh(f_xr, x, h_ind, F):
    for i in range(len(x)):
        if i == h_ind:
            continue
        if f_xr <= F.evaluate(x[i]):
            return False

    return True


def get_contraction_point(xh, xc, beta):
    d = len(xc)
    xk = [(1 - beta) * xc[i] + beta * xh[i] for i in range(d)]

    return xk[:]


def get_points_shifted_towards_xl(x, xl, sigma):
    d = len(xl)
    n = len(x)
    x = [[(1 - sigma) * xl[i] + sigma * x[j][i] for i in range(d)] for j in range(n)]

    return x


def simplex_stopping_criterion_ok(f_xc, x, e, F):
    n = len(x)
    dev = sqrt(sum([(F.evaluate(xi) - f_xc) ** 2 for xi in x]) / n)
    return dev <= e


def simplex_search_method(f_goal, h, x0, alfa, beta, gama, sigma, e, is_multi_input=True):
    e_default = 1e-6
    h_default = 1
    alfa_default = 1
    beta_default = 0.5
    gama_default = 2
    sigma_default = 0.5

    if e is None:
        e = e_default
    if h is None:
        h = h_default
    if alfa is None:
        alfa = alfa_default
    if beta is None:
        beta = beta_default
    if gama is None:
        gama = gama_default
    if sigma is None:
        sigma = sigma_default

    x = get_simplex_points(x0, h)
    # print("x =",x)

    F = GoalFunction(f_goal, is_multi_input=is_multi_input)

    while True:
        x_evaluated = [F.evaluate(xi) for xi in x]

        h_ind = get_index_h(x_evaluated)
        xh = x[h_ind]
        l_ind = get_index_l(x_evaluated)
        xl = x[l_ind]

        xc = get_centroid(x, h_ind)
        f_xc = None
        if print_it:
            f_xc = F.evaluate(xc, count_eval=False)  # count_eval=False jer se F(xc) racuna samo radi prikaza, ne treba za algoritam
            print("xc =", xc, "f(xc) =", f_xc)
        if print_to_file:
            f_xc = F.evaluate(xc, count_eval=False)
            file_for_print.write(f"xc = {xc} f(xc) = {f_xc}\n")

        xr = get_reflection_point(xh, xc, alfa)
        f_xr = F.evaluate(xr)
        # print("xr =", xr, "f(xr) =", f_xr)

        f_xl = F.evaluate(xl)
        # print("xl =", xl, "f(xl) =", f_xl)

        if f_xr < f_xl:
            xe = get_expansion_point(xr, xc, gama)

            f_xe = F.evaluate(xe)
            # print("xe =", xe, "f(xe) =", f_xe)

            if f_xe < f_xl:
                x[h_ind] = xe[:]
                xh = xe[:]
            else:
                x[h_ind] = xr[:]
                xh = xr[:]
        else:
            if is_xr_worse_than_all_except_xh(f_xr, x, h_ind, F):
                f_xh = F.evaluate(xh)
                # print("xh =", xh, "f(xh) =", f_xh)
                if f_xr < f_xh:
                    x[h_ind] = xr[:]
                    xh = xr[:]
                    f_xh = F.evaluate(xh)

                xk = get_contraction_point(xh, xc, beta)
                f_xk = F.evaluate(xk)
                # print("xk =", xk, "f(xk) =", f_xk)

                if f_xk < f_xh:
                    x[h_ind] = xk[:]
                    xh = xk[:]
                else:
                    x = get_points_shifted_towards_xl(x, xl, sigma)[:]
            else:
                x[h_ind] = xr[:]

        # print("x =",x)
        if f_xc is None:
            f_xc = F.evaluate(xc)
        if simplex_stopping_criterion_ok(f_xc, x, e, F):
            break

    x_best = xl

    return F.called_times, (x_best if len(x_best) > 1 else x_best[0])


def simplex_search(params_file_name, f_goal, is_multi_input=True):
    with open(params_file_name, "r") as simplex_params_file:
        h = alfa = beta = gama = sigma = e = None
        lines = simplex_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = float(l[1].strip())
            elif l[0].strip() == "h":
                h = float(l[1].strip())
            elif l[0].strip() == "alfa":
                alfa = float(l[1].strip())
            elif l[0].strip() == "beta":
                beta = float(l[1].strip())
            elif l[0].strip() == "gama":
                gama = float(l[1].strip())
            elif l[0].strip() == "sigma":
                sigma = float(l[1].strip())
            else:  # l[0].strip() == "x0"
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]

    F_called_times, x_solution = simplex_search_method(f_goal, h, x0, alfa, beta, gama, sigma, e,
                                                       is_multi_input=is_multi_input)

    return F_called_times, x_solution


def hooke_jeeves_stopping_criterion_not_ok(dx, e):
    n = len(e)
    for i in range(n):
        if dx[i] > e[i]:
            return True
    return False


def hooke_jeeves_explore(F, xp, dx):
    x = xp[:]
    n = len(x)
    for i in range(n):
        p = F.evaluate(x)
        x[i] += dx[i]
        n = F.evaluate(x)
        if n > p:
            x[i] -= 2 * dx[i]
            n = F.evaluate(x)
            if n > p:
                x[i] += dx[i]
    return x


def hooke_jeeves_method(f_goal, dx, x0, e, is_multi_input=True):
    e_default = 1e-6
    dx_default = 0.5

    if e is None:
        e = [e_default for _ in range(len(x0))]
    if dx is None:
        dx = [dx_default for _ in range(len(x0))]

    F = GoalFunction(f_goal, is_multi_input=is_multi_input)

    n = len(x0)

    xb = x0[:]
    xp = x0[:]

    while hooke_jeeves_stopping_criterion_not_ok(dx, e):
        xn = hooke_jeeves_explore(F, xp, dx)

        if print_it:
            print()
            f_xb = F.evaluate(xb, count_eval=False)
            f_xp = F.evaluate(xp, count_eval=False)
            f_xn = F.evaluate(xn, count_eval=False)
            print(f"xb={xb}; f(xb)={f_xb}")
            print(f"xp={xp}; f(xp)={f_xp}")
            print(f"xn={xn}; f(xn)={f_xn}")
        if print_to_file:
            file_for_print.write("\n")
            f_xb = F.evaluate(xb, count_eval=False)
            f_xp = F.evaluate(xp, count_eval=False)
            f_xn = F.evaluate(xn, count_eval=False)
            file_for_print.write(f"xb={xb}; f(xb)={f_xb}\n")
            file_for_print.write(f"xp={xp}; f(xp)={f_xp}\n")
            file_for_print.write(f"xn={xn}; f(xn)={f_xn}\n")

        if F.evaluate(xn) < F.evaluate(xb):
            xp = [(2 * xn[i] - xb[i]) for i in range(n)]
            xb = xn[:]
        else:
            dx = [0.5 * dx[i] for i in range(n)]
            xp = xb[:]

    return F.called_times, (xb if len(xb) > 1 else xb[0])


def hooke_jeeves(params_file_name, f_goal, is_multi_input=True):
    with open(params_file_name, "r") as simplex_params_file:
        dx = e = None
        lines = simplex_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = l[1].strip().split()
                e = [float(el) for el in e]
            elif l[0].strip() == "dx":
                dx = l[1].strip().split()
                dx = [float(el) for el in dx]
            else:  # l[0].strip() == "x0"
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]

    F_called_times, x_solution = hooke_jeeves_method(f_goal, dx, x0, e, is_multi_input=is_multi_input)

    return F_called_times, x_solution


def print_x_fx_1_dim(f, x, F_called_times):
    print("x =", x, "; f =", f(x))
    file_for_print.write(f"x = {x}; f = {f(x)}\n")
    print("F_called_times =", F_called_times)
    file_for_print.write(f"F_called_times = {F_called_times}\n")


def print_x_fx(f, x, F_called_times, unpack_x=True):
    if unpack_x:
        fx = f(*x)
        print("x =", *x, "; f =", fx)
        file_for_print.write(f"x = {x}; f = {fx}\n")
    else:
        fx = f(x)
        print("x =", *x, "; f =", fx)
        file_for_print.write(f"x = {x}; f = {fx}\n")
    print("F_called_times =", F_called_times)
    file_for_print.write(f"F_called_times = {F_called_times}\n")

    return fx


def zad1():
    print("------------------------ ZAD 1 ------------------------")
    file_for_print.write("------------------------ ZAD 1 ------------------------\n")
    # xmin = 3, fmin = 0
    # x0 = 10
    f31 = lambda x: (x - 3) ** 2

    print("golden_ratio")
    file_for_print.write("golden_ratio\n")
    F_called_times, x = golden_ratio(f31)
    print_x_fx_1_dim(f31, x, F_called_times)

    print()
    print("coordinate_search")
    file_for_print.write("\ncoordinate_search\n")
    F_called_times, x = coordinate_search("koord_params_0.txt", f31)
    print_x_fx_1_dim(f31, x, F_called_times)

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")
    F_called_times, x = simplex_search("simplex_params_0.txt", f31)
    print_x_fx_1_dim(f31, x, F_called_times)

    print()
    print("hooke_jeeves")
    file_for_print.write("\nhooke_jeeves\n")
    F_called_times, x = hooke_jeeves("hooke_jeeves_params_0.txt", f31)
    print_x_fx_1_dim(f31, x, F_called_times)

    x0_lst = [[10], [50], [-50], [100], [-100], [1000], [-1000]]

    print("***********************")
    file_for_print.write("***********************\n")
    print("Razliciti x0:")
    file_for_print.write("Razliciti x0:\n")
    for x0 in x0_lst:
        print("-----------")
        file_for_print.write("-----------\n")
        print("x0 =", x0[0])
        file_for_print.write(f"x0 = {x0[0]}\n")

        print("golden_ratio")
        file_for_print.write("\ngolden_ratio\n")
        F = GoalFunction(f31)
        F_called_times, xa, xb = golden_ratio_with_point(F=F, h=1, x0=x0[0], e=1e-6)
        print_x_fx_1_dim(f31, (xa + xb) / 2, F_called_times)

        print()
        print("coordinate_search")
        file_for_print.write("\ncoordinate_search\n")
        F_called_times, x = coordinate_search_method(f31, h=[1], x0=x0, e=[1e-6])
        print_x_fx_1_dim(f31, x, F_called_times)

        print()
        print("simplex_search")
        file_for_print.write("\nsimplex_search\n")
        F_called_times, x = simplex_search_method(f_goal=f31, h=1, x0=x0, alfa=1, beta=0.5, gama=2, sigma=0.5, e=1e-6)
        print_x_fx_1_dim(f31, x, F_called_times)

        print()
        print("hooke_jeeves")
        file_for_print.write("\nhooke_jeeves\n")
        F_called_times, x = hooke_jeeves_method(f_goal=f31, dx=[0.5], x0=x0, e=[1e-6])
        print_x_fx_1_dim(f31, x, F_called_times)


def zad2():
    print()
    print("------------------------ ZAD 2 ------------------------")
    file_for_print.write("------------------------ ZAD 2 ------------------------\n")
    # xmin = (1,1), fmin = 0
    # x0 = (-1.9, 2)
    f1 = lambda x1, x2: 100 * (x2 - x1 ** 2) ** 2 + (1 - x1) ** 2

    # xmin = (4,2), fmin = 0
    # x0 = (0.1, 0.3)
    f2 = lambda x1, x2: (x1 - 4) ** 2 + 4 * (x2 - 2) ** 2

    # xmin=(1,2,3,4,5), fmin = 0
    # x0 = (0, 0, 0, 0, 0)
    f3 = lambda x1, x2, x3, x4, x5: (x1 - 1) ** 2 + (x2 - 2) ** 2 + (x3 - 3) ** 2 + (x4 - 4) ** 2 + (x5 - 5) ** 2
    # f3 = lambda x: sum([(x[j] - (j + 1)) ** 2 for j in range(len(x))])

    # xmin = (0,0), fmin = 0
    # x0 = (5.1, 1.1)
    f4 = lambda x1, x2: abs((x1 - x2) * (x1 + x2)) + sqrt(x1 ** 2 + x2 ** 2)

    F_called_times_f1 = []
    fx_f1 = []
    x_f1 = []
    print("******* f1 *******")
    file_for_print.write("******* f1 *******\n")
    print()
    print("coordinate_search")
    file_for_print.write("\ncoordinate_search\n")
    F_called_times, x = coordinate_search("koord_params_1.txt", f1)
    x_f1.append(x)
    f1_x = print_x_fx(f1, x, F_called_times)
    F_called_times_f1.append(F_called_times)
    fx_f1.append(f1_x)

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")
    F_called_times, x = simplex_search("simplex_params_1.txt", f1)
    x_f1.append(x)
    f1_x = print_x_fx(f1, x, F_called_times)
    F_called_times_f1.append(F_called_times)
    fx_f1.append(f1_x)

    print()
    print("hooke_jeeves")
    file_for_print.write("\nhooke_jeeves\n")
    F_called_times, x = hooke_jeeves("hooke_jeeves_params_1.txt", f1)
    x_f1.append(x)
    f1_x = print_x_fx(f1, x, F_called_times)
    F_called_times_f1.append(F_called_times)
    fx_f1.append(f1_x)
    print()

    F_called_times_f2 = []
    fx_f2 = []
    x_f2 = []
    print("******* f2 *******")
    file_for_print.write("\n******* f2 *******\n")
    print()
    print("coordinate_search")
    file_for_print.write("\ncoordinate_search\n")
    F_called_times, x = coordinate_search("koord_params_2.txt", f2)
    x_f2.append(x)
    f2_x = print_x_fx(f2, x, F_called_times)
    F_called_times_f2.append(F_called_times)
    fx_f2.append(f2_x)

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")
    F_called_times, x = simplex_search("simplex_params_2.txt", f2)
    x_f2.append(x)
    f2_x = print_x_fx(f2, x, F_called_times)
    F_called_times_f2.append(F_called_times)
    fx_f2.append(f2_x)

    print()
    print("hooke_jeeves")
    file_for_print.write("\nhooke_jeeves\n")
    F_called_times, x = hooke_jeeves("hooke_jeeves_params_2.txt", f2)
    x_f2.append(x)
    f2_x = print_x_fx(f2, x, F_called_times)
    F_called_times_f2.append(F_called_times)
    fx_f2.append(f2_x)
    print()

    F_called_times_f3 = []
    fx_f3 = []
    x_f3 = []
    print("******* f3 *******")
    file_for_print.write("\n******* f3 *******\n")
    print()
    print("coordinate_search")
    file_for_print.write("\ncoordinate_search\n")
    F_called_times, x = coordinate_search("koord_params_3.txt", f3)
    f3_x = print_x_fx(f3, x, F_called_times)
    # F_called_times, x = coordinate_search("koord_params_3.txt", f3, is_iterative_f_goal=True)
    # f3_x = print_x_fx(f3, x, F_called_times, unpack_x=False)
    x_f3.append(x)
    F_called_times_f3.append(F_called_times)
    fx_f3.append(f3_x)

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")
    F_called_times, x = simplex_search("simplex_params_3.txt", f3)
    f3_x = print_x_fx(f3, x, F_called_times)
    # F_called_times, x = simplex_search("simplex_params_3.txt", f3, is_multi_input=False)
    # f3_x = print_x_fx(f3, x, F_called_times, unpack_x=False)
    x_f3.append(x)
    F_called_times_f3.append(F_called_times)
    fx_f3.append(f3_x)

    print()
    print("hooke_jeeves")
    file_for_print.write("\nhooke_jeeves\n")
    F_called_times, x = hooke_jeeves("hooke_jeeves_params_3.txt", f3)
    f3_x = print_x_fx(f3, x, F_called_times)
    # F_called_times, x = hooke_jeeves("hooke_jeeves_params_3.txt", f3, is_multi_input=False)
    # f3_x = print_x_fx(f3, x, F_called_times, unpack_x=False)
    x_f3.append(x)
    F_called_times_f3.append(F_called_times)
    fx_f3.append(f3_x)
    print()

    F_called_times_f4 = []
    fx_f4 = []
    x_f4 = []
    print("******* f4 *******")
    file_for_print.write("\n******* f4 *******\n")
    print()
    print("coordinate_search")
    file_for_print.write("\ncoordinate_search\n")
    F_called_times, x = coordinate_search("koord_params_4.txt", f4)
    x_f4.append(x)
    f4_x = print_x_fx(f4, x, F_called_times)
    F_called_times_f4.append(F_called_times)
    fx_f4.append(f4_x)

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")
    F_called_times, x = simplex_search("simplex_params_4.txt", f4)
    x_f4.append(x)
    f4_x = print_x_fx(f4, x, F_called_times)
    F_called_times_f4.append(F_called_times)
    fx_f4.append(f4_x)

    print()
    print("hooke_jeeves")
    file_for_print.write("\nhooke_jeeves\n")
    F_called_times, x = hooke_jeeves("hooke_jeeves_params_4.txt", f4)
    x_f4.append(x)
    f4_x = print_x_fx(f4, x, F_called_times)
    F_called_times_f4.append(F_called_times)
    fx_f4.append(f4_x)
    print()
    file_for_print.write("\n")

    print("F called times:")
    file_for_print.write("F called times:\n")
    print("    coordinate     simplex        hooke-jeeves  ")
    file_for_print.write("    coordinate     simplex        hooke-jeeves  \n")
    print("f1: %-15d%-15d%-15d" % (F_called_times_f1[0], F_called_times_f1[1], F_called_times_f1[2]))
    file_for_print.write("f1: %-15d%-15d%-15d\n" % (F_called_times_f1[0], F_called_times_f1[1], F_called_times_f1[2]))
    print("f2: %-15d%-15d%-15d" % (F_called_times_f2[0], F_called_times_f2[1], F_called_times_f2[2]))
    file_for_print.write("f2: %-15d%-15d%-15d\n" % (F_called_times_f2[0], F_called_times_f2[1], F_called_times_f2[2]))
    print("f3: %-15d%-15d%-15d" % (F_called_times_f3[0], F_called_times_f3[1], F_called_times_f3[2]))
    file_for_print.write("f3: %-15d%-15d%-15d\n" % (F_called_times_f3[0], F_called_times_f3[1], F_called_times_f3[2]))
    print("f4: %-15d%-15d%-15d" % (F_called_times_f4[0], F_called_times_f4[1], F_called_times_f4[2]))
    file_for_print.write("f4: %-15d%-15d%-15d\n" % (F_called_times_f4[0], F_called_times_f4[1], F_called_times_f4[2]))
    print("xmin:")
    file_for_print.write("xmin:\n")
    print("    coordinate                              simplex                                 hooke-jeeves  ")
    file_for_print.write("    coordinate                              simplex                                 hooke-jeeves  \n")
    print(*x_f1)
    file_for_print.write(f"{x_f1}\n")
    print(*x_f2)
    file_for_print.write(f"{x_f2}\n")
    print(*x_f3)
    file_for_print.write(f"{x_f3}\n")
    print(*x_f4)
    file_for_print.write(f"{x_f4}\n")

    print("F(xmin):")
    file_for_print.write("F(xmin):\n")
    print("    coordinate             simplex                hooke-jeeves  ")
    file_for_print.write("    coordinate             simplex                hooke-jeeves  \n")
    print("f1: %-.15e  %-.15e  %-.15e" % (fx_f1[0], fx_f1[1], fx_f1[2]))
    file_for_print.write("f1: %-.15e  %-.15e  %-.15e\n" % (fx_f1[0], fx_f1[1], fx_f1[2]))
    print("f2: %-.15e  %-.15e  %-.15e" % (fx_f2[0], fx_f2[1], fx_f2[2]))
    file_for_print.write("f2: %-.15e  %-.15e  %-.15e\n" % (fx_f2[0], fx_f2[1], fx_f2[2]))
    print("f3: %-.15e  %-.15e  %-.15e" % (fx_f3[0], fx_f3[1], fx_f3[2]))
    file_for_print.write("f3: %-.15e  %-.15e  %-.15e\n" % (fx_f3[0], fx_f3[1], fx_f3[2]))
    print("f4: %-.15e  %-.15e  %-.15e" % (fx_f4[0], fx_f4[1], fx_f4[2]))
    file_for_print.write("f4: %-.15e  %-.15e  %-.15e\n" % (fx_f4[0], fx_f4[1], fx_f4[2]))


def zad3():
    print()
    print("------------------------ ZAD 3 ------------------------")
    file_for_print.write("------------------------ ZAD 3 ------------------------\n")
    # xmin = (0,0), fmin = 0
    # x0 = (5, 5)
    f4 = lambda x1, x2: abs((x1 - x2) * (x1 + x2)) + sqrt(x1 ** 2 + x2 ** 2)

    print("coordinate_search")
    file_for_print.write("coordinate_search\n")
    F_called_times, x = coordinate_search("koord_params_4_z3.txt", f4)
    f4_x = print_x_fx(f4, x, F_called_times)

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")
    F_called_times, x = simplex_search("simplex_params_4_z3.txt", f4)
    f4_x = print_x_fx(f4, x, F_called_times)

    print()
    print("hooke_jeeves")
    file_for_print.write("\nhooke_jeeves\n")
    F_called_times, x = hooke_jeeves("hooke_jeeves_params_4_z3.txt", f4)
    f4_x = print_x_fx(f4, x, F_called_times)
    print()
    file_for_print.write("\n")


def zad4():
    print("------------------------ ZAD 4 ------------------------")
    file_for_print.write("------------------------ ZAD 4 ------------------------\n")
    # xmin = (1,1), fmin = 0
    # x0 = (-1.9, 2)
    f1 = lambda x1, x2: 100 * (x2 - x1 ** 2) ** 2 + (1 - x1) ** 2

    print()
    print("simplex_search")
    file_for_print.write("\nsimplex_search\n")

    x0_lst = [[0.5, 0.5], [20, 20]]
    h_lst = range(1, 21)
    for x0 in x0_lst:
        print("-----------")
        file_for_print.write("-----------\n")
        print("x0 =", x0)
        file_for_print.write(f"x0 = {x0}\n")

        for h in h_lst:
            print("h =", h)
            file_for_print.write(f"h = {h}\n")
            F_called_times, x = simplex_search_method(f_goal=f1, h=h, x0=x0, alfa=1, beta=0.5, gama=2, sigma=0.5,
                                                      e=1e-6)
            print_x_fx(f1, x, F_called_times)


def zad5():
    print()
    print("------------------------ ZAD 5 ------------------------")
    file_for_print.write("------------------------ ZAD 5 ------------------------\n")
    f6 = lambda x1, x2: 0.5 + ((sin(sqrt(x1 ** 2 + x2 ** 2))) ** 2 - 0.5) / (1 + 0.001 * (x1 ** 2 + x2 ** 2)) ** 2

    located_min_count_simplex = 0
    located_min_count_hooke_jeeves = 0
    for i in range(10000):
        x1 = uniform(-50, 50)
        x2 = uniform(-50, 50)
        F_called_times, (x1, x2) = simplex_search_method(f_goal=f6, h=None, x0=[x1, x2], alfa=1, beta=0.5, gama=2,
                                                         sigma=0.5, e=1e-6)
        f6_val_min = f6(x1, x2)
        if f6_val_min < 1e-4:
            located_min_count_simplex += 1

        F_called_times, (x1, x2) = hooke_jeeves_method(f_goal=f6, dx=[0.5, 0.5], x0=[x1, x2], e=[1e-6, 1e-6])
        f6_val_min = f6(x1, x2)
        if f6_val_min < 1e-4:
            located_min_count_hooke_jeeves += 1
    print("simplex_search hooke_jeeves_search")
    file_for_print.write("simplex_search hooke_jeeves_search\n")
    print("located_min_count =", located_min_count_simplex, located_min_count_hooke_jeeves)
    file_for_print.write(f"located_min_count = {located_min_count_simplex} {located_min_count_hooke_jeeves}\n")

    # print("simplex_search")
    # located_min_count_simplex = 0
    # for i in range(10000):
    #     x1 = uniform(-50, 50)
    #     x2 = uniform(-50, 50)
    #     F_called_times, (x1, x2) = simplex_search_method(f_goal=f6, h=None, x0=[x1,x2], alfa=1, beta=0.5, gama=2, sigma=0.5, e=1e-6)
    #     f6_val_min = f6(x1, x2)
    #     if f6_val_min < 1e-4:
    #         located_min_count_simplex += 1
    # print("located_min_count =",located_min_count_simplex)

    # print("hooke_jeeves_search")
    # located_min_count_hooke_jeeves = 0
    # for i in range(10000):
    #     x1 = uniform(-50, 50)
    #     x2 = uniform(-50, 50)
    #     F_called_times, (x1, x2) = hooke_jeeves_method(f_goal=f6, dx=[0.5, 0.5], x0=[x1, x2], e=[1e-6, 1e-6])
    #     f6_val_min = f6(x1, x2)
    #     if f6_val_min < 1e-4:
    #         located_min_count_hooke_jeeves += 1
    # print("located_min_count =", located_min_count_hooke_jeeves)

    # jako dugo treba za neke (x1, x2)
    # print("coordinate_search")
    # located_min_count_coordinate = 0
    # for i in range(1000):
    #     print(i)
    #     x1 = uniform(-50, 50)
    #     x2 = uniform(-50, 50)
    #     F_called_times, (x1, x2) = coordinate_search_method(f6, h=[1, 1], x0=[x1, x2], e=[1e-6, 1e-6])
    #     f6_val_min = f6(x1, x2)
    #     if f6_val_min < 1e-4:
    #         located_min_count_coordinate += 1
    # print("located_min_count =", located_min_count_coordinate)


if __name__ == "__main__":
    with open("yn_print_algorithm_steps.txt", "r") as should_print_alg_steps_file:
        lines = should_print_alg_steps_file.readlines()
        lines = [int(l.strip()) for l in lines]
        print_it = lines[0] == 1
        print_to_file = lines[1] == 1
        # print(print_it)
        # print(print_to_file)

        file_for_print = "zad1_out.txt"
        with open("zad1_out.txt", "w") as file_for_print:
            zad1()

        file_for_print = "zad2_out.txt"
        with open("zad2_out.txt", "w") as file_for_print:
            zad2()

        file_for_print = "zad3_out.txt"
        with open("zad3_out.txt", "w") as file_for_print:
            zad3()

        file_for_print = "zad4_out.txt"
        with open("zad4_out.txt", "w") as file_for_print:
            zad4()

        file_for_print = "zad5_out.txt"
        with open("zad5_out.txt", "w") as file_for_print:
            zad5()
