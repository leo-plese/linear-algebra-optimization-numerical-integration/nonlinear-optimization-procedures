Programsko okruženje: PyCharm 2019.2.4
Programski jezik: Python 3.8

Upute za pokretanje:
U programskom okruženju se pokrene glavni program (bez argumenata) smješten u datoteci "main.py" sa Shift+F10 ili na tipku "Run 'main'".
Pokretanjem se izvršavaju svi zadaci iz vježbe.
Rezultat svakog zadatka se ispisuje na ekran i zapisuje u datoteke odgovarajućeg imena (imena datoteka vidljiva su iz glavnog programa).
Ako se želi dobiti detaljan ispis provedbe algoritama onda se u datoteci yn_print_algorithm_steps.txt:
- detaljni ispis na ekranu: u 1. liniji umjesto 0 postaviti na 1
- detaljni ispis u odgovarajuću izlaznu ("zad{i}_out.txt", i = 1,2,3,4,5) datoteku zadatka: u 2. liniji umjesto 0 postaviti na 1

Napomena:
Izlazne datoteke zadataka ("zad{i}_out.txt", i = 1,2,3,4,5) nisu priložene jer bi bila prevelika .zip arhiva za upload na Ferko.
Po potrebi se pokretanjem programa mogu rekreirati tako da se u datoteci yn_print_algorithm_steps.txt u 2. liniji postavi 1 (vidjeti uputu gore).
