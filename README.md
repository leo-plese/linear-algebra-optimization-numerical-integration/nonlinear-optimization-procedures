Nonlinear Optimization Procedures.

Topics:

- Golden ratio procedure

- Search by coordinate axes

- Nelder-Mead Simplex procedure

- Hooke-Jeeves procedure

Implemented in Python.

My lab assignment in Computer Aided Analysis and Design, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2020
